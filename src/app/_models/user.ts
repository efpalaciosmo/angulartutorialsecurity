export interface User {
  id: Number,
  firstName: string,
  lastName: string,
  username: string,
  password: string,
  token?: string,
}
