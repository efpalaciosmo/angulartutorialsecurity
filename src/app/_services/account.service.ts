import {Injectable} from "@angular/core";
import {BehaviorSubject, map, materialize, Observable} from "rxjs";
import {User} from "@app/_models";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {environment} from "@environments/environment";

@Injectable({ providedIn: 'root'})
export class AccountService {
  userSubject: BehaviorSubject<User | null>;
  user: Observable<User | null>;

  constructor(private router: Router, private http: HttpClient) {
    this.userSubject = new BehaviorSubject<User | null>(JSON.parse(localStorage.getItem('user')!));
    this.user = this.userSubject.asObservable();
  }

  public get userValue(){
    return this.userSubject.value;
  }

  login(username: string, password: string): Observable<User>{
    return this.http.post<User>(`${environment.apiUrl}/users/authenticate`, {username, password})
      .pipe(map(user => {
        localStorage.setItem('user', JSON.stringify(user));
        this.userSubject.next(user);
        return user;
      }))
  }
  logout(): void{
    // remove user from local storage and set current user to null
    localStorage.removeItem('user');
    this.userSubject.next(null);
    this.router.navigate(['/account/login'])
  }

  register(user: User){
    return this.http.post(`${environment.apiUrl}/users/register`, user)
  }
}
