import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./app.component";
import { HomeComponent } from './home';
import { RegisterComponent, LoginComponent } from './account'
import {AppRoutingModule} from "./app-routing-module";
import {ErrorInterceptor, fakeBackendProvider, JwtInterceptor} from "./_helpers";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, ReactiveFormsModule],
  declarations: [AppComponent, HomeComponent, RegisterComponent, LoginComponent],
  bootstrap: [AppComponent],
  providers: [
    // this tell to angular dependency injection system how to get a value for a dependency
    // multi: true tell to add the provider to the collection of HTTP_INTERCEPTORS
    fakeBackendProvider,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
  ]
})
export class AppModule {}
