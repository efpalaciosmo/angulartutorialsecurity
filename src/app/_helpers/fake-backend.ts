import {User} from "../_models/user";
import {
  HTTP_INTERCEPTORS,
  HttpBackend,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from "@angular/common/http";
import {delay, dematerialize, materialize, Observable, of, throwError} from "rxjs";

// array in local storage for registered users
const usersKey: string = 'angular-tutorial-users'
let users: any[] = JSON.parse(localStorage.getItem(usersKey)!) || [];
export class FakeBackendInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const { url, method, headers, body } = req;
    return handleRouter();
    function handleRouter(){
      switch (true){
        case url.endsWith('/users/authenticate') && method == 'POST':
          return authenticate();
        case url.endsWith('/users/register') && method==='POST':
          return register();
        default:
          // pass through any request not handled
          return next.handle(req);
      }
    }

    // Route functions
    function authenticate() {
      const { username, password } = body;
      const user = users.find(x => x.username===username && x.password===password);
      if(!user){
        return error("username or password is incorrect");
      }
      return ok({
        ...basicDetails(user),
        token: 'fake-jwt-token'}
      )
    }

    function register() {
      const user = body;
      if(users.find(x => x.username === user.username)){
        return error(`Username ${user.username} is already taken`)
      }
      user.id = users.length ? Math.max(...users.map(x => x.id)) + 1: 1;
      users.push(user);
      localStorage.setItem(usersKey, JSON.stringify(users));
      return ok();
    }

    // Helper functions

    function ok(body? : any){
      return of(new HttpResponse({status: 200, body}))
        .pipe(delay(500)); // Delay observable to simulate server api call
    }

    function error(message: string){
      return throwError(() => ({error: {message}}))
        .pipe(materialize(), delay(500), dematerialize())  // call materialize and dematerialize to ensure delay even if an error is thrown
    }

    function basicDetails(user: User) {
      const { id, username, firstName, lastName} = user;
      return { id, username, firstName, lastName};
    }
  }
}

export const fakeBackendProvider = {
  // use fake backend in place of Http service for backend-less development
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
}
