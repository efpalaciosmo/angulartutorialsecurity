import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {AccountService} from "@app/_services";
import {catchError, Observable, throwError} from "rxjs";
import {Injectable} from "@angular/core";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private accountService: AccountService) {}

  // auto logout if 401 or 403 response returned from api
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
      .pipe(catchError(err => {
        if([401, 403].includes(err.status) && this.accountService.userSubject){
          // auto logout if 401 or 403 response returned from api
          this.accountService.logout();
        }
        const error = err.error?.message || err.statusText;
        console.log(err);
        return throwError(() => error);
      }))
  }
}
