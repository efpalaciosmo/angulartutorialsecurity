import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {AccountService} from "@app/_services";
import {Observable} from "rxjs";
import {environment} from "@environments/environment";
import {Injectable} from "@angular/core";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private accountService: AccountService) {}

  // adds and http authorization header with JWT token to the headers of all
  // requests to the API URL if the user is authenticated
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const user = this.accountService.userValue;
    const isLoggedIn: string | undefined = user?.token;
    const isApiUrl = req.url.startsWith(environment.apiUrl);
    if(isLoggedIn && isApiUrl){
      req = req.clone({
        setHeaders: { Authorization: `Bearer ${user?.token}`
    }});
    }
    return next.handle(req);
  }
}
